package com.nicopaez.tutorials.junit;

import org.junit.Assert;
import org.junit.Test;
import java.util.HashMap;

import static org.hamcrest.CoreMatchers.is;

public class ChatCounterTest {

    @Test
    public void countAllShouldReturnEmptyHashWhenEmptyString(){
        CharCounter charCounter = new CharCounter();
        HashMap<Character, Integer> results = charCounter.countAll("");
        Assert.assertTrue(results.isEmpty());
    }

    @Test
    public void countAllShouldReturnEmptyHashWhenNull(){
        CharCounter charCounter = new CharCounter();
        HashMap<Character, Integer> results = charCounter.countAll(null);
        Assert.assertTrue(results.isEmpty());
    }

    @Test
    public void countAllShouldReturnHashCharCount(){
        CharCounter charCounter = new CharCounter();
        HashMap<Character, Integer> results = charCounter.countAll("hola");
        Assert.assertThat(1, is(results.get('h')));
        Assert.assertThat(1, is(results.get('o')));
        Assert.assertThat(1, is(results.get('l')));
        Assert.assertThat(1, is(results.get('a')));
    }

    @Test
    public void countAllShouldIgnoreCasing(){
        CharCounter charCounter = new CharCounter();
        HashMap<Character, Integer> results = charCounter.countAll("hOLa");
        Assert.assertThat(1, is(results.get('h')));
        Assert.assertThat(1, is(results.get('o')));
        Assert.assertThat(1, is(results.get('l')));
        Assert.assertThat(1, is(results.get('a')));
    }

    @Test
    public void countAllShouldCountRepeatedChars(){
        CharCounter charCounter = new CharCounter();
        HashMap<Character, Integer> results = charCounter.countAll("hello");
        Assert.assertThat(1, is(results.get('h')));
        Assert.assertThat(1, is(results.get('e')));
        Assert.assertThat(2, is(results.get('l')));
    }

    @Test
    public void countAllShouldReturn0WhenNoChar(){
        CharCounter charCounter = new CharCounter();
        HashMap<Character, Integer> results = charCounter.countAll("hello");
        Assert.assertThat(0, is(results.get('x')));
    }


}
