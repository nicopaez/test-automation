package com.nicopaez.tutorials.junit;

import org.junit.Assert;
import org.junit.Test;

import java.util.Stack;

public class StackTest {

    @Test
    public void emptyShouldBeTrueWhenCreated() {
        Stack stack = new Stack();

        boolean result = stack.empty();

        Assert.assertTrue(result);

    }

    @Test
    public void emptyShouldBeFalseWhenElementAdded(){
        Stack stack = new Stack();
        stack.push(new Integer(1));
        boolean result = stack.empty();
        Assert.assertFalse(result);
    }

    @Test
    public void popShouldReturnLastElementPushed(){
        Stack stack = new Stack();
        Object obj1 = new Integer(1);
        Object obj2 = new Integer(2);
        stack.push(obj1);
        stack.push(obj2);

        Integer result = (Integer)stack.pop();

        Assert.assertEquals(result, obj2);
    }
}
