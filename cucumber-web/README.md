Cucumber Selenium Sample
========================

This sample shows how to use cucumber + Selenium web driver to create acceptance tests for a simple web application. 

It is based on Java 1.7 and can be compiled and run using Maven by executing mvm test.

For the test to be executed successfully you need internet connection because it tests 2 web apps:

* http://job-vacancy-staging.herokuapp.com
* http://www.metric-conversions.org/temperature/celsius-to-fahrenheit.htm

To run the tests execute the following command:

````
mvn test
````
For feedback or support please contact nicopaez_at_computer_dot_org.

To show the result of the test in Jenkins you can use the Cucumber Reports Plugin: https://wiki.jenkins-ci.org/display/JENKINS/Cucumber+Reports+Plugin