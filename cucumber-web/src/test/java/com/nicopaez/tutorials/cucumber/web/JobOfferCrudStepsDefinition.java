package com.nicopaez.tutorials.cucumber.web;

import org.junit.Assert;
import org.openqa.selenium.By;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class JobOfferCrudStepsDefinition {
	
	private final SharedDriver driver;

	public JobOfferCrudStepsDefinition(SharedDriver sharedDriver) {
		this.driver = sharedDriver;
	}
	
	@Given("^I am logged in as job offerer$")
	public void i_am_logged_in_as_job_offerer() throws Throwable {
		driver.getRelative("/login");
	    driver.findElement(By.name("user[email]")).clear();
	    driver.findElement(By.name("user[email]")).sendKeys("offerer@test.com");
	    driver.findElement(By.name("user[password]")).clear();
	    driver.findElement(By.name("user[password]")).sendKeys("Passw0rd!");
	    driver.findElement(By.id("loginButton")).click();
	    Assert.assertTrue(driver.getPageSource().contains("offerer@test.com"));
	}

	@Given("^I access the new offer page$")
	public void i_access_the_new_offer_page() throws Throwable {
		driver.getRelative("/job_offers/new");
	}

	@When("^I fill the title with \"(.*?)\"$")
	public void i_fill_the_title_with(String offerTitle) throws Throwable {
	    driver.findElement(By.name("job_offer[title]")).clear();
	    driver.findElement(By.name("job_offer[title]")).sendKeys(offerTitle);
	}

	@When("^confirm the new offer$")
	public void confirm_the_new_offer() throws Throwable {
		driver.findElement(By.id("createButton")).click();
	}

	@Then("^I should see \"(.*?)\"$")
	public void i_should_see(String someText) throws Throwable {
	    Assert.assertTrue(driver.getPageSource().contains(someText));
	}

	@Then("^I should see \"(.*?)\" in My Offers$")
	public void i_should_see_in_My_Offers(String someText) throws Throwable {
		driver.getRelative("/job_offers/my");
		Assert.assertTrue(driver.getPageSource().contains(someText));
	}

	@Given("^I have \"(.*?)\" offer in My Offers$")
	public void i_have_offer_in_My_Offers(String offerTitle) throws Throwable {
		driver.getRelative("/job_offers/new");  
	    driver.findElement(By.name("job_offer[title]")).clear();
	    driver.findElement(By.name("job_offer[title]")).sendKeys(offerTitle);
	    driver.findElement(By.id("createButton")).click();
	}

	@Given("^I edit it$")
	public void i_edit_it() throws Throwable {
		driver.findElement(By.linkText("Edit")).click();
	}

	@Given("^I set title to \"(.*?)\"$")
	public void i_set_title_to(String offerTitle) throws Throwable {
	    driver.findElement(By.name("job_offer[title]")).clear();
	    driver.findElement(By.name("job_offer[title]")).sendKeys(offerTitle);
	}

	@Given("^I save the modification$")
	public void i_save_the_modification() throws Throwable {
	    driver.findElement(By.id("saveButton")).click();
	}

	@Given("^I delete it$")
	public void i_delete_it() throws Throwable {
	    driver.findElement(By.xpath("(//button[@type='submit'])[1]")).click();
	}

	@Then("^I should not see \"(.*?)\" in My Offers$")
	public void i_should_not_see_in_My_Offers(String offerTitle) throws Throwable {
		driver.getRelative("/job_offers/my");
		Assert.assertFalse(driver.getPageSource().contains(offerTitle));
	}
	
}