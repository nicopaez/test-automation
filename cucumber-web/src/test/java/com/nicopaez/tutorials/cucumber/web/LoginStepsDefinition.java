package com.nicopaez.tutorials.cucumber.web;

import org.junit.Assert;
import org.openqa.selenium.By;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginStepsDefinition {

	private final SharedDriver driver;
	 
	public LoginStepsDefinition(SharedDriver sharedDriver) {
		this.driver = sharedDriver;
	}
	
	@Given("^I visit login page$")
	public void i_visit_login_page() throws Throwable {
		driver.getRelative("/login");
	}

	@When("^I use credentials \"(.*?)\" and \"(.*?)\"$")
	public void i_user_credentials_and(String user, String password) throws Throwable {
	    driver.findElement(By.name("user[email]")).clear();
	    driver.findElement(By.name("user[email]")).sendKeys(user);
	    driver.findElement(By.name("user[password]")).clear();
	    driver.findElement(By.name("user[password]")).sendKeys(password);
	    driver.findElement(By.id("loginButton")).click();
	}
	
	@Then("^I should see My Offers$")
	public void i_should_see_My_Offers() throws Throwable {
		Assert.assertTrue(driver.getPageSource().contains("My Offers"));
	}

	@Then("^I should see Invalid credentials$")
	public void i_should_see_Invalid_credentials() throws Throwable {
		Assert.assertTrue(driver.getPageSource().contains("Invalid credentials"));
	}

    @Given("^a user with credentials \"([^\"]*)\" and \"([^\"]*)\"$")
    public void aUserWithCredentialsAnd(String username, String password) throws Throwable {
		// Nothing needs to be done here for now
    }
}
