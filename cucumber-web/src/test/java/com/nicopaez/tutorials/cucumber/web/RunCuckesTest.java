package com.nicopaez.tutorials.cucumber.web;



import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = false, features={"src/test/features/"}, strict = true, tags={"~@wip"}, format = {
		"pretty",
		"html:target/reports/cucumber",
		"json:target/reports/cucumber.json",
		"junit:target/reports/cucumber.xml"
		})
public class RunCuckesTest {

}
