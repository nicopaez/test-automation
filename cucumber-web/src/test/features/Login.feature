Feature: Login

  Background:
    Given a user with credentials "offerer@test.com" and "Passw0rd!"

  Scenario: Successful login
    Given I visit login page
    When I use credentials "offerer@test.com" and "Passw0rd!"
    Then I should see My Offers
    
  Scenario: Fail login
    Given I visit login page
    When I use credentials "offerer@test.com" and "zazara"
    Then I should see Invalid credentials