package com.nicopaez.tutorials.cucumber;

public enum AuthenticationResult {

	SUCCESS,
	FAILED,
	LOCKED
	
}
