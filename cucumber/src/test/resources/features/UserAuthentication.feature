Feature: User Authentication

  Scenario: Successful authentication
    Given I am not authenticated
     When I authenticate with credentials "john@someplace" and "myPassw0rd!"
     Then I am authenticated

  @wip
  Scenario: Invalid credentials and failed authentication 
    Given I am not authenticated
     When I authenticate with credentials "john@someplace" and "wrong_password"
     Then I am not authenticated
  
  @wip   
  Scenario: Account locked after 3 failed authentication attempts
    Given I am not authenticated
     When I authenticate with credentials "john@someplace" and "wrong_password" 3 times
     Then my account is locked
     
     