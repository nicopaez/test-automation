package com.nicopaez.tutorials.cucumber;

import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import static org.junit.Assert.*;

public class AuthenticationSteps {

	AuthenticationResult authResult;

	@When("^I authenticate with credentials \"(.*?)\" and \"(.*?)\"$")
	public void i_authenticate_with_credentials_and(String username, String password) throws Throwable {
		AuthenticationService authService = new AuthenticationService();
		authResult = authService.authenticate(username, password);
	}

	@Then("^I am authenticated$")
	public void authenticated() throws Throwable {
		assertEquals(AuthenticationResult.SUCCESS, authResult);
	}

	@Then("^I am not authenticated$")
	public void notAuthenticated() throws Throwable {
		assertNotEquals(AuthenticationResult.SUCCESS, authResult);
	}

	@Then("^my account is locked$")
	public void accountLocked() throws Throwable {
		assertEquals(AuthenticationResult.LOCKED, authResult);
	}
}
