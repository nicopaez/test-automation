package fitnesse.websamples;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class LoginDriver {

	private WebDriver driver = null;//new FirefoxDriver();
	static final String HOST_URL = "http://job-vacancy-staging.herokuapp.com"; 

	private String getAbsoluteUrl(String relativeUrl) {
		return HOST_URL + relativeUrl;
	}

	public LoginDriver() {
		driver = new FirefoxDriver();
		driver.get(this.getAbsoluteUrl("/health/reset"));
	}
	
	public void userAndPassword(String user, String password) {
		driver.get(this.getAbsoluteUrl("/login"));
	    driver.findElement(By.name("user[email]")).clear();
	    driver.findElement(By.name("user[email]")).sendKeys(user);
	    driver.findElement(By.name("user[password]")).clear();
	    driver.findElement(By.name("user[password]")).sendKeys(password);
	    driver.findElement(By.id("loginButton")).click();   
	}
	
	public String profile() {
		String profile = driver.findElement(By.className("profile")).getText();
		this.driver.close();
		return profile;
	}
	
	public String flashMessage() {
		String profile = driver.findElement(By.id("flashMessage")).getText();
		this.driver.close();
		return profile.substring(0,19);
	}

}
