
This sample is based on [FitNesse Launcher Plugin samples](https://code.google.com/p/fitnesse-launcher-maven-plugin/).

To run FitNesse as a wiki server execute:

`mvn clean verify -P wiki`

And browse:  [http://localhost:9123/WebSamples]( http://localhost:9123/WebSamples)

To run FitNesse as automation integration tests execute:

`mvn clean verify -P auto`


 
