package com.nicopaez.notas;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Alumno {

    private final String nombre;
    private Map<String, Nota> mapaNotas = new HashMap<String, Nota>();

    public Alumno(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return this.nombre;
    }

    public Nota registrarNota(Evaluacion evaluacion, float valorNota) {
        Nota nota = new Nota(this, evaluacion, valorNota);
        this.mapaNotas.put(evaluacion.getId(), nota);
        return nota;
    }

    public Optional<Nota> obtenerNota(Evaluacion evaluacion) {
        return Optional.ofNullable(this.mapaNotas.get(evaluacion.getId()));
    }
}
