package com.nicopaez.notas;

/**
 * Created by nicopaez on 16/09/2018.
 */
public class Nota {
    private final Alumno alumno;
    private final Evaluacion evaluacion;
    private final float valor;

    public Nota(Alumno alumno, Evaluacion evaluacion, float valor) {
        this.alumno = alumno;
        this.evaluacion = evaluacion;
        this.valor = valor;
    }

    public float getValor() {
        return valor;
    }
}
