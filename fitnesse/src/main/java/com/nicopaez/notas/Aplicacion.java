package com.nicopaez.notas;

import java.util.Optional;

public class Aplicacion {

    private static Curso curso;

    public Aplicacion() {
        curso = new Curso();
    }

    public float calcularNotaFinal(String nombreAlumno) {
        Optional<Alumno> alumno = curso.getAlumno(nombreAlumno);
        return curso.calcularNotaFinal(alumno.get());
    }

    public void registrarNotaTarea(String nombreAlumno, String id_tarea, float valorNota) {
        Optional<Alumno> alumno = curso.getAlumno(nombreAlumno);
        if (!alumno.isPresent()) {
            alumno = Optional.of(this.curso.nuevoAlumno(nombreAlumno));
        }
        Evaluacion eval = this.curso.getEvaluacion(id_tarea);
        alumno.get().registrarNota(eval,valorNota);
    }

}
