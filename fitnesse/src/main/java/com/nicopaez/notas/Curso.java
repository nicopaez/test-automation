package com.nicopaez.notas;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class Curso {

    private static final float NOTA_MINIMA_TAREAS = 4.0f;
    private static final float NOTA_DESAPROBACION = 2.0f;
    private static final float NOTA_MINIMA_TPS = 6.0f;
    private Set<Alumno> alumnos = new HashSet<Alumno>();
    private Set<Evaluacion> tps = new HashSet<Evaluacion>();
    private Set<Evaluacion> tareas = new HashSet<Evaluacion>();

    public Curso() {
        this.tareas.add(new Evaluacion("t1"));
        this.tareas.add(new Evaluacion("t2"));
        this.tareas.add(new Evaluacion("t3"));
        this.tps.add(new Evaluacion("tp1"));
        this.tps.add(new Evaluacion("tp2"));
    }

    public Alumno nuevoAlumno(String nombre) {
        Alumno nuevoAlumno = new Alumno(nombre);
        this.alumnos.add(nuevoAlumno);
        return nuevoAlumno;
    }

    public Set<Evaluacion> getEvaluaciones() {
        HashSet<Evaluacion> evaluaciones = new HashSet<Evaluacion>();
        evaluaciones.addAll(this.tareas);
        evaluaciones.addAll(this.tps);
        return evaluaciones;
    }

    public float calcularNotaFinal(Alumno alumno) {
        float acumTareas = 0;
        float acumTps = 0;
        for(Evaluacion eval : this.tareas) {
            Optional<Nota> nota = alumno.obtenerNota(eval);
            if (!nota.isPresent()) {
                return NOTA_DESAPROBACION;
            }
            if (nota.get().getValor() < NOTA_MINIMA_TAREAS) {
                return NOTA_DESAPROBACION;
            }
            else {
                acumTareas += nota.get().getValor();
            }
        }

        for(Evaluacion eval : this.tps) {
            Optional<Nota> nota = alumno.obtenerNota(eval);
            if (!nota.isPresent()) {
                return NOTA_DESAPROBACION;
            }
            if (nota.get().getValor() < NOTA_MINIMA_TPS) {
                return NOTA_DESAPROBACION;
            }
            else {
                acumTps += nota.get().getValor();
            }
        }
        float promTareas = acumTareas / this.tareas.size();
        float promTps = acumTps / this.tps.size();
        return  promTps * 0.6f + promTareas * 0.4f;
    }

    public Evaluacion getEvaluacion(String idEvaluacion) {
        Optional<Evaluacion> resultado = this.getEvaluaciones().stream().filter(obj->obj.getId().equals(idEvaluacion)).findFirst();
        return resultado.get();
    }

    public Optional<Alumno> getAlumno(String nombreAlumno) {
        return this.alumnos.stream().filter(obj -> obj.getNombre().equals(nombreAlumno)).findFirst();
    }
}
