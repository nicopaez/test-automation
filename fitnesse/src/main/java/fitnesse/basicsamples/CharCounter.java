package fitnesse.basicsamples;


import java.util.HashMap;

public class CharCounter {
	
	private String word;
	private static int SYMBOL_MONEY = -1;
	private static int SYMBOL_AT = -2;
	
	public CharCounter(String word) {
		this.word = word;
	}

    public int howMany(char charToCount) {    	
    	return this.countAll().get(charToCount);
    }
    
    public HashMap<Character,Integer> countAll() {
    	HashMap<Character, Integer> counts = new HashMap<Character, Integer>();
    	this.initializeHashMap(counts);
    	
    	for (int i = 0; i < this.word.length(); i++) {
    		char key = this.word.charAt(i);
    		int previousValue = counts.get(key);
    		if (previousValue >= 0) {
    			counts.put(key, previousValue + 1);
    		} else {
    			counts.put(key, 1);
    		}			
    	}
    	
    	return counts;
    }
    
    private void initializeHashMap(HashMap<Character, Integer> hashMap) {
    	for (int c = 1; c < 128; c++) {
    		hashMap.put((char)c, 0);  		
    	}
    	
    	hashMap.put('$', SYMBOL_MONEY);
    	hashMap.put('@', SYMBOL_AT);
    }
    
}
