!2 Funcionalidad: calculo de la nota final del curso

!3 Contexto
El siguiente diagrama muestra los conceptos centrales de este dominio:

!img https://lh3.googleusercontent.com/MC86C3Zz0M3LiwsxQjOjiqxCPJcSRLAJpCmg2VUHgMu9-8MRBeQOP4OHE7ZJH6eQFeDnYNPRj-fmHA=w2560-h1406

Notas complementarias del dominio:
* Cada curso consta de 3 tareas y 2 Tps
* Las tareas son de indole individual suele durar 1 semana. Están enfocada en temás puntuales
* Los Tps son grupales y su duración puede ser de varias semanas. Entán enfocadas en la integración de conceptos.

!3 Reglas
La nota final del curso obedece a las siguiente reglas:
1. Deben estar aprobadas (nota > 4) todas las tareas (condición necesaria)
2. Deben estar aprobados (nota > 6) los dos trabajos prácticos (condición necesaria)
3. Si no se cumplen las condiciones necesarias la nota final se considera un 2
4. La nota final se calcula como (0.4 * promedio TPs)+(0.6 * promedio TPs), asumiendo que se cumplen 1 y 2

!3 Ejemplos
!*> '''glue code'''
|import |
|fitnesse.basicsamples|
*!

!|AprobacionFixture|
| alumno | tarea1 | tarea2 | tarea3 | tp1 | tp2 | notaFinal? |
| Juan Aprobador | 7      |   7    | 7 | 8 | 8 | 7.6 |
| Mateo Nohizolaarea | 1      |   7    | 7 | 8 | 8 | 2 |
| Julian AlBorde | 4      |   4    | 4 | 6 | 6 | 5.2 |
| Maria Nohizountp | 4      |   4    | 4 | 6 | 1 | 2 |
| Carla Lamejor | 4      |   4    | 4 | 6 | 1 | 2 |
