|import |
|fitnesse.basicsamples|
 
This application is really simple, it just take an input word and allow to count how many times a character is included in the input word.
 
!|CharCounterFixture|
| input | char | howMany? |
| hola  |  x   | 0        |
| hola  |  a   | 1        |
| hello |  l   | 2        |
