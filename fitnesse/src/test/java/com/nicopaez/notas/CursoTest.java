package com.nicopaez.notas;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CursoTest {

    @Test
    public void debeRegistarAlumnos() {
        Curso curso = new Curso();
        Alumno alumno = curso.nuevoAlumno("Juan");
        assertNotNull(alumno);
    }

    @Test
    public void debeTener5Evaluaciones() {
        Curso curso = new Curso();
        assertEquals(5, curso.getEvaluaciones().size());
    }

    @Test
    public void debeDevolverEvaluaciones() {
        Curso curso = new Curso();
        Evaluacion tp1 = curso.getEvaluacion("tp1");
        assertNotNull(tp1);
    }

    @Test
    public void debeCalcularNotaFinal() {
        Curso curso = new Curso();
        Alumno alumnoJuan = curso.nuevoAlumno("Juan");
        float notaFinal = curso.calcularNotaFinal(alumnoJuan);
        assertNotNull(notaFinal);
    }

    @Test
    public void notalFinalConsideraTareasYTPs() {
        Curso curso = new Curso();
        Alumno alumnoJuan = curso.nuevoAlumno("Juan");
        alumnoJuan.registrarNota(curso.getEvaluacion("t1"), 7);
        alumnoJuan.registrarNota(curso.getEvaluacion("t2"), 7);
        alumnoJuan.registrarNota(curso.getEvaluacion("t3"), 7);
        alumnoJuan.registrarNota(curso.getEvaluacion("tp1"), 8);
        alumnoJuan.registrarNota(curso.getEvaluacion("tp2"), 8);
        float notaFinal = curso.calcularNotaFinal(alumnoJuan);
        assertEquals(7.6, notaFinal, 0.01);
    }

    @Test
    public void notaFinalDebeSer2SinoAproboTodasLasTareas() {
        Curso curso = new Curso();
        Alumno alumnoJuan = curso.nuevoAlumno("Juan");
        float notaFinal = curso.calcularNotaFinal(alumnoJuan);
        assertEquals(2.0, notaFinal, 0.0);
    }
}
