package com.nicopaez.notas;

import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class AlumnoTest {

    @Test
    public void debeRegistrarSusNotas() {
        float valorNota = 7.0f;
        Alumno alumno = new Alumno("Juan");
        Evaluacion evaluacion = new Evaluacion("t1");
        alumno.registrarNota(evaluacion, valorNota);

        Optional<Nota> notaT1= alumno.obtenerNota(evaluacion);

        assertEquals(valorNota, notaT1.get().getValor(), 0.0f);
    }
}
