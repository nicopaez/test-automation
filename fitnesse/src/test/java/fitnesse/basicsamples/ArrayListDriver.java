package fitnesse.basicsamples;

import java.util.ArrayList;

public class ArrayListDriver {

	@SuppressWarnings("rawtypes")
	private ArrayList list = new ArrayList();
	
	@SuppressWarnings("unchecked")
	public void add(int element) {
		this.list.add(element);
	}
	
	public void remove(int element) {
		this.list.remove(element);
	}
	
	public int size() {
		return this.list.size();
	}
}
