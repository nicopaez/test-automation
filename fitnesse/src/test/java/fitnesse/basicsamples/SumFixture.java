package fitnesse.basicsamples;

public class SumFixture {
	
	private int operand1;
	private int operand2;

	public void setOperand1(int value) {
		this.operand1 = value;
	}

	public void setOperand2(int value) {
		this.operand2 = value;
	}
	
	public int result() {
		return this.operand1 + this.operand2;
	}
}
