package fitnesse.basicsamples;

import fitnesse.basicsamples.CharCounter;

public class CharCounterFixture {

	private String input;
	private char targetChar;

	public void setInput(String input) {
		this.input = input;
	}

	public void setChar(char targetChar) {
		this.targetChar = targetChar;
	}

	public int howMany() {
		CharCounter counter = new CharCounter(this.input);
		return counter.howMany(this.targetChar);
	}
}
