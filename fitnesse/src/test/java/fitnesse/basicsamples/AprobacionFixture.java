package fitnesse.basicsamples;

import com.nicopaez.notas.Aplicacion;

import java.text.DecimalFormat;


public class AprobacionFixture {

    private final Aplicacion aplicacion;
    private String nombreAlumno;

    public AprobacionFixture() {
        this.aplicacion = new Aplicacion();
    }

    public void setAlumno(String nombreAlumno) {
        this.nombreAlumno = nombreAlumno;
    }

    public void setTarea1(int nota) {
        this.aplicacion.registrarNotaTarea(nombreAlumno, "t1", nota);
    }

    public void setTarea2(int nota) {
        this.aplicacion.registrarNotaTarea(nombreAlumno, "t2", nota);
    }

    public void setTarea3(int nota) {
        this.aplicacion.registrarNotaTarea(nombreAlumno, "t3", nota);

    }

    public void setTp1(int nota) {
        this.aplicacion.registrarNotaTarea(nombreAlumno, "tp1", nota);

    }

    public void setTp2(int nota) {
        this.aplicacion.registrarNotaTarea(nombreAlumno, "tp2", nota);
    }

    public String notaFinal() {
        float nota = this.aplicacion.calcularNotaFinal(nombreAlumno);
        DecimalFormat formatter = new DecimalFormat("##.#");
        return formatter.format(nota);
    }
}
