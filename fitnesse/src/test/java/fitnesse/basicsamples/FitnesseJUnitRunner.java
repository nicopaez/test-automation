package fitnesse.basicsamples;

import fitnesse.junit.FitNesseRunner;
import org.junit.runner.RunWith;

import fitnesse.junit.FitNesseRunner.FitnesseDir;
import fitnesse.junit.FitNesseRunner.OutputDir;
import fitnesse.junit.FitNesseRunner.Suite;


@RunWith(FitNesseRunner.class)
@FitNesseRunner.Suite("BasicSamples")
@FitNesseRunner.FitnesseDir("target/fitnesse")
@FitNesseRunner.OutputDir("fitnesseOutput")
public class FitnesseJUnitRunner {
}
