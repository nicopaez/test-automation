package fitnesse.basicsamples;

public class StringFixture {

	private String input;

	public void setInput(String input) {
		this.input = input;
	}
	
	public int size() {
		return this.input.length();
	}
}
