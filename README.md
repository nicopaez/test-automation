# Test automation tutorial

Contents:

* junit: [![Build Status](https://nicopaez.ci.cloudbees.com/view/test-automation/job/junit/badge/icon)](https://nicopaez.ci.cloudbees.com/view/test-automation/job/junit/)
* junit-mockito: [![Build Status](https://nicopaez.ci.cloudbees.com/view/test-automation/job/junit-mockito/badge/icon)](https://nicopaez.ci.cloudbees.com/view/test-automation/job/junit-mockito/)
* junit-webdriver: [![Build Status](https://nicopaez.ci.cloudbees.com/view/test-automation/job/junit-webdriver/badge/icon)](https://nicopaez.ci.cloudbees.com/view/test-automation/job/junit-webdriver/)
* cucumber: [![Build Status](https://nicopaez.ci.cloudbees.com/view/test-automation/job/cucumber/badge/icon)](https://nicopaez.ci.cloudbees.com/view/test-automation/job/cucumber/)
* cucumber-web: [![Build Status](https://nicopaez.ci.cloudbees.com/view/test-automation/job/cucumber-web/badge/icon)](https://nicopaez.ci.cloudbees.com/view/test-automation/job/cucumber-web/)
* fitnesse: [![Build Status](https://nicopaez.ci.cloudbees.com/view/test-automation/job/fitnesse/badge/icon)](https://nicopaez.ci.cloudbees.com/view/test-automation/job/fitnesse/)
* fitnesse-web: [![Build Status](https://nicopaez.ci.cloudbees.com/view/test-automation/job/fitnesse-web/badge/icon)](https://nicopaez.ci.cloudbees.com/view/test-automation/job/fitnesse-web/)
* jmeter: [![Build Status](https://nicopaez.ci.cloudbees.com/view/test-automation/job/jmeter/badge/icon)](https://nicopaez.ci.cloudbees.com/view/test-automation/job/jmeter/)

Associated slides are: https://docs.google.com/presentation/d/1A99dA4zS0A_y2asNN7izwIF3wKS5RoRG5eKjsqfFTWk/edit#slide=id.g44153ab4e_02