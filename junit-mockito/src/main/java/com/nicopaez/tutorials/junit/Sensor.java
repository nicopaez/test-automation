package com.nicopaez.tutorials.junit;

public interface Sensor {

	boolean isInContact();
}
